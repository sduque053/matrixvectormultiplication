library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;


package mat_pak is

	type matrix 		is array (natural range <>, natural range <>) of unsigned(3 downto 0);
	type vector 		is array (natural range <>)						 of unsigned(3 downto 0);
	type vector_long 	is array (natural range <>)						 of unsigned(7 downto 0);

	
end mat_pak;