library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.mat_pak.all;

entity MVM is
	generic(
		M:positive:=4; --TAMAÑO DEL VECTOR DE SALIDA
		N:positive:=4  --TAMAÑO DEL VECTOR DE MULTIPLICACION
	);
	port(
			en : in std_logic;
			mat:	in matrix(N-1 downto 0,M-1 downto 0);
			vec:	in vector(M-1 downto 0);
			mv:	out vector_long(N-1 downto 0)
	);
end entity MVM;

architecture RTL of MVM is

signal a : matrix(N-1 downto 0, M-1 downto 0);
signal b : vector(M-1 downto 0);
signal c : vector_long(N-1 downto 0);

begin

	a<=mat;
	b<=vec;
	c<=mv;
	
	process(en)

	begin
		
		for i in 0 to N-1 loop
		
			for j in 0 to M-1 loop
					
					c(i) <= c(i)+(a(i,j) * b(j));
					
			  end loop;
		
		end loop;
		 
	end process;
	
	 
end RTL;